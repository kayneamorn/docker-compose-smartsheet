#!/usr/bin/python
import os
import requests

rows_url = "https://api.smartsheet.com/2.0/sheets/%s/rows" % os.environ["SHEET_ID"]
columns_url = "https://api.smartsheet.com/2.0/sheets/%s/columns" % os.environ["SHEET_ID"]
headers = {'Authorization': 'Bearer %s' % os.environ["SMARTSHEET_API"]}

r = requests.get(columns_url, headers=headers).json()
total_columns = r['totalCount']
column_ids = []

for i in range(total_columns):
    column_ids.append(r['data'][i]['id'])

data = [{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue1"},{"columnId":column_ids[1],"value":"Open"},{"columnId":column_ids[2],"value":"Red"},{"columnId":column_ids[3],"value":"Exists in production","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue2"},{"columnId":column_ids[1],"value":"Resolved"},{"columnId":column_ids[2],"value":"Yellow"},{"columnId":column_ids[3],"value":"Found in staging environment","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue3"},{"columnId":column_ids[1],"value":"Closed"},{"columnId":column_ids[2],"value":"Green"},{"columnId":column_ids[3],"value":"Found in testing environment","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue4"},{"columnId":column_ids[1],"value":"Resolved"},{"columnId":column_ids[2],"value":"Yellow"},{"columnId":column_ids[3],"value":"Comment4","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue5"},{"columnId":column_ids[1],"value":"Open"},{"columnId":column_ids[2],"value":"Yellow"},{"columnId":column_ids[3],"value":"Comment5","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue6"},{"columnId":column_ids[1],"value":"Closed"},{"columnId":column_ids[2],"value":"Red"},{"columnId":column_ids[3],"value":"Comment6","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue7"},{"columnId":column_ids[1],"value":"Closed"},{"columnId":column_ids[2],"value":"Yellow"},{"columnId":column_ids[3],"value":"Comment7","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue8"},{"columnId":column_ids[1],"value":"Resolved"},{"columnId":column_ids[2],"value":"Green"},{"columnId":column_ids[3],"value":"Comment8","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue9"},{"columnId":column_ids[1],"value":"Open"},{"columnId":column_ids[2],"value":"Green"},{"columnId":column_ids[3],"value":"Comment9","strict":"false"}]},{"toBottom":"true","cells":[{"columnId":column_ids[0],"value":"Issue10"},{"columnId":column_ids[1],"value":"Resolved"},{"columnId":column_ids[2],"value":"Red"},{"columnId":column_ids[3],"value":"Comment10","strict":"false"}]}]
r = requests.post(rows_url, json=data, headers=headers)

if r.json()['message'] == "SUCCESS" and r.json()['resultCode'] == 0:
    print("The POST response returned a " + r.json()['message'] + " message and resultCode of: " + str(r.json()['resultCode']))
else:
    raise Exception("Exception thrown: " + r.text)
