#!/usr/bin/env bash
set -eu

chmod +x /usr/local/src/install_pip.sh
chmod +x /usr/local/src/delete_all_rows.py

/usr/local/src/install_pip.sh
/usr/local/src/delete_all_rows.py