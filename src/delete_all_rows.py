#!/usr/bin/python
import os
import requests

url = "https://api.smartsheet.com/2.0/sheets/%s" % os.environ["SHEET_ID"]
headers = {'Authorization': 'Bearer %s' % os.environ["SMARTSHEET_API"]}

# API call to get the total number of rows and row IDs
r = requests.get(url, headers=headers).json()
total_rows = r['totalRowCount']

# Use list comprehension to print out row_ids for delete/cleanup--only execute when rows exist
if total_rows > 0:
    list_of_all_rows = (','.join(str(r['rows'][i]['id']) for i in range(total_rows)))
    url_with_rows_to_delete = url + "/rows?ids=%s" % list_of_all_rows
    r = requests.delete(url_with_rows_to_delete, headers=headers)
    if str(r)=='<Response [200]>':
        print('Deleted ' + str(total_rows) + ' rows successfully.')
    else:
        raise Exception('Exception thrown' + r.text)
else:
    print('Found 0 rows, nothing to delete.')