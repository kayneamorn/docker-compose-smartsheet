### Note: this is an old file that was previously used to add rows via curl
### It has been deprecated for a pythonic way that can pass in columnIds using list indices
### The file is only kept around as an example of using curl with Smartsheet API

#!/usr/bin/env bash
set -eu

apt-get update
apt-get install -y curl

status=$(curl https://api.smartsheet.com/2.0/sheets/$SHEET_ID/rows \
-H "Authorization: Bearer $SMARTSHEET_API" \
-H "Content-Type: application/json" \
-X POST \
-d '[{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue1"},{"columnId":852128634824580,"value":"Open"},{"columnId":5355728262195076,"value":"Red"},{"columnId":3103928448509828,"value":"Exists in production","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue2"},{"columnId":852128634824580,"value":"Resolved"},{"columnId":5355728262195076,"value":"Yellow"},{"columnId":3103928448509828,"value":"Found in staging environment","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue3"},{"columnId":852128634824580,"value":"Closed"},{"columnId":5355728262195076,"value":"Green"},{"columnId":3103928448509828,"value":"Found in testing environment","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue4"},{"columnId":852128634824580,"value":"Resolved"},{"columnId":5355728262195076,"value":"Yellow"},{"columnId":3103928448509828,"value":"Comment4","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue5"},{"columnId":852128634824580,"value":"Open"},{"columnId":5355728262195076,"value":"Yellow"},{"columnId":3103928448509828,"value":"Comment5","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue6"},{"columnId":852128634824580,"value":"Closed"},{"columnId":5355728262195076,"value":"Red"},{"columnId":3103928448509828,"value":"Comment6","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue7"},{"columnId":852128634824580,"value":"Closed"},{"columnId":5355728262195076,"value":"Yellow"},{"columnId":3103928448509828,"value":"Comment7","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue8"},{"columnId":852128634824580,"value":"Resolved"},{"columnId":5355728262195076,"value":"Green"},{"columnId":3103928448509828,"value":"Comment8","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue9"},{"columnId":852128634824580,"value":"Open"},{"columnId":5355728262195076,"value":"Green"},{"columnId":3103928448509828,"value":"Comment9","strict":false}]},{"toBottom":true,"cells":[{"columnId":5918678215616388,"value":"Issue10"},{"columnId":852128634824580,"value":"Resolved"},{"columnId":5355728262195076,"value":"Red"},{"columnId":3103928448509828,"value":"Comment10","strict":false}]}]')
messageStatus=$(echo $status | tr -d '{}' | sed 's/,/ /g' | awk '{print $1}')
resultCode=$(echo $status | tr -d '{}' | sed 's/,/ /g' | awk '{print $2}')

if [ "$messageStatus" == "\"message\":\"SUCCESS\"" ] && [ "$resultCode" == "\"resultCode\":0" ]
then
  echo "The curl command executed successfully."
else
  echo "The curl command failed with the response:"
  echo $status
  exit 1
fi