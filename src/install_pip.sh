#!/usr/bin/env bash
set -eu

apt-get update && apt-get -y install python-pip
pip install requests