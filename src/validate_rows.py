#!/usr/bin/python
import os
import requests

url = "https://api.smartsheet.com/2.0/sheets/%s" % os.environ["SHEET_ID"]
headers = {'Authorization': 'Bearer %s' % os.environ["SMARTSHEET_API"]}

r = requests.get(url, headers=headers).json()
total_rows = r['totalRowCount']

def validate_row(i, issue, status, ryg, comments):
    row_issue = r['rows'][i]['cells'][0]['displayValue']
    row_status = r['rows'][i]['cells'][1]['displayValue']
    row_ryg = r['rows'][i]['cells'][2]['displayValue']
    row_comments = r['rows'][i]['cells'][3]['displayValue']
    if row_issue==issue and row_status==status and row_ryg==ryg and row_comments==comments:
        print('Row ' + str(i+1) + ' validation successful.')
    else:
        raise Exception('Row ' + str(i+1) + ' validation failed--Expected row to contain: ' + issue + ', ' + status + ', ' + ryg + ', ' + comments)
    return

validate_row(0, "Issue1", "Open", "Red", "Exists in production")
validate_row(1, "Issue2", "Resolved", "Yellow", "Found in staging environment")
validate_row(2, "Issue3", "Closed", "Green", "Found in testing environment")
validate_row(3, "Issue4", "Resolved", "Yellow", "Comment4")
validate_row(4, "Issue5", "Open", "Yellow", "Comment5")
validate_row(5, "Issue6", "Closed", "Red", "Comment6")
validate_row(6, "Issue7", "Closed", "Yellow", "Comment7")
validate_row(7, "Issue8", "Resolved", "Green", "Comment8")
validate_row(8, "Issue9", "Open", "Green", "Comment9")
validate_row(9, "Issue10", "Resolved", "Red", "Comment10")