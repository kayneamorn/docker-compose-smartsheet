# Docker-in-Docker (dind) with docker-compose on Bitbucket Pipelines

[docker-compose](https://docs.docker.com/compose/) is a python
application installed via [pip](https://pip.pypa.io/en/stable/).

## Proof of concept with dind and docker-compose

This POC uses 3 containers that each perform very specific functions.
The first container adds 10 rows of sample data to a specific Smartsheet via the curl command.
The second container validates that the 10 rows are populated with the correct values using a custom Python function.
The third container does a cleanup and deletes all the rows from the Smartsheet.

This is a fail-fast pipeline. Example:  if there are any errors or exceptions found in the first container, the pipeline will fail
immediately--the second and third container will not launch.

Please note that the amount of containers used in this project is unnecessary, but merely used as a proof of concept
of dind and docker-compose, along with the integration of Smartsheet API.