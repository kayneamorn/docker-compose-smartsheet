### This is an example code that can be pushed out into Azure (serverless) Functions
### where it can run on a cron interval to iterate and monitor for specific cell matches
### If the cells match certain criteria, it can then send a webhook to Twilio to SMS or call
### a phone number with a customized voice message using Twilio markup language

### NOTE:  The Twilio work has not been implemented and only has placeholder text at this time

#!/usr/bin/python
import requests

url = "https://api.smartsheet.com/2.0/sheets/%s" % os.environ["SHEET_ID"]
headers = {'Authorization': 'Bearer %s' % os.environ["SMARTSHEET_API"]}
r = requests.get(url, headers=headers).json()
total_rows = r['totalRowCount']

alert_twilio = False

def no_matches_found(row_number):
    return('Found no matches for row: ' + str(row_number + 1))

for i in range(total_rows):
     try:
         row_status = r['rows'][i]['cells'][3]['displayValue']
     except KeyError:
         print(no_matches_found(i))
         continue
     try:
         row_ryg = r['rows'][i]['cells'][4]['displayValue']
     except KeyError:
         print(no_matches_found(i))
         continue
     try:
         row_comments = r['rows'][i]['cells'][5]['displayValue']
     except KeyError:
         print(no_matches_found(i))
         continue

     if row_status=="Open" and row_ryg=="Red" and "production" in row_comments.lower():
         alert_twilio = True
         print('Match found for row: ' + str(i+1))
     else:
         print(no_matches_found(i))

if alert_twilio:
    print('Placeholder: This is where you would send a Twilio webhook to trigger SMS or programmable voice with TwiML')